import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        String value1;
        String value2;

        Scanner input = new Scanner(System.in);
        try {   
            System.out.print("Masukan angka 1 : ");
            value1 = input.nextLine();
            System.out.print("Masukan angka 2 : ");
            value2 = input.nextLine();
            StringBuilder sb = new StringBuilder();
            StringBuilder sb1 = new StringBuilder();

            for (char ch : value1.toCharArray()) {
                if (Character.isDigit(ch)) {
                    sb.append(ch);
                }
            }
            for (char ch : value2.toCharArray()) {
                if (Character.isDigit(ch)) {
                    sb1.append(ch);
                }
            }
            int a = Integer.parseInt(sb.toString());
            int b = Integer.parseInt(sb1.toString());
            int result = a + b;
            System.out.println(result);
        } catch (NumberFormatException nfe) {
            System.out.println("Error : " + nfe.toString());
            nfe.printStackTrace();
        }
        input.close();
    }
}