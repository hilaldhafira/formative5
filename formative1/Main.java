import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Main {
    public static void createFolder(File folder) {
        if (!folder.exists()) {
            System.out.println("Membuat folder " + folder);
            folder.mkdir();
        } else {
            System.out.println("Folder sudah terbuat");
        }
    }

    public static void main(String[] args) throws IOException {
        String text[];

        text = new String[10];
        for (int i = 0; i <= 9; i++) {
            text[i] = "Hello Java " + (i+1);
        }

        String namaFolder = "form1";
        File folder = new File(namaFolder);
        createFolder(folder);
        for (int i = 0; i < text.length; i++) {
            FileWriter file = new FileWriter("form1/output."+(i+1)+".txt");
            try (BufferedWriter bw = new BufferedWriter(file)) {
                bw.write(text[i]);
                bw.close();
        } catch (IOException e) {
            e.getStackTrace();
        }
        }
    }
}
