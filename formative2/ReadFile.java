import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ReadFile {
    static String url = "C:/Bootcamp/Formative/Jawaban/formative5/formative1/Form1/";

    public static void getFolderWithText() throws FileNotFoundException {
        File folder = new File(url);
        File[] files = folder.listFiles();
        for (File file : files) {
            System.out.println("Nama file : " + file.getName());
            getTextFile(file.getName());
        }
    }
    public static void getTextFile(String file) throws FileNotFoundException {
        File content = new File(url + file);
        Scanner readFile = new Scanner(content);
        while (readFile.hasNextLine()) {
            String text = readFile.nextLine();
            System.out.println("Isi File = " + text);
        }
        readFile.close();
    }
    public static void main(String[] args) throws Exception {

        getFolderWithText();

    }
}
